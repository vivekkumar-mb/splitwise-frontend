import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { HttpserviceService } from '../services/httpservice.service';

@Component({
  selector: 'app-friend-data',
  templateUrl: './friend-data.component.html',
  styleUrls: ['./friend-data.component.css'],
})
export class FriendDataComponent implements OnInit {
  public friendId: any;
  public friendName: any;
  public allFriendExpenses: any = {};
  public expenses: any = {};
  public userId: any = localStorage.getItem('userId');
  constructor(
    private route: ActivatedRoute,
    private apiService: HttpserviceService
  ) {}

  ngOnInit(): void {
    // this.friendName = this.route.snapshot.params.friendName;
    // this.friendId = this.route.snapshot.params.friendId;
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.friendName = params.get('friendName');
      this.friendId = params.get('friendId');
      this.expenses = {};
      this.apiService
        .getFriendAllExpenses(this.friendId, this.userId)
        .subscribe((response: any) => {
          // console.log(response);
          if (response.data === undefined) {
            this.allFriendExpenses = [];
          } else {
            this.allFriendExpenses = response.data.expenses;
          }
          for (let eachEx of this.allFriendExpenses) {
            const expenseDesc = eachEx['expenseDesc'];
            const payerArr = JSON.parse(eachEx['payer']);

            for (let obj of payerArr) {
              const x = Object.entries(obj);
              // console.log(x);
              if (x[0][0] === this.userId) {
                this.expenses[expenseDesc] = [x[0][1], eachEx['expenseAmount']];
                //data:{biryani:[100,480]}
              }
            }
          }
          this.expenses = Object.entries(this.expenses);
          console.log(this.expenses);
        });
    });
    // this.apiService
    //   .getFriendAllExpenses(this.friendId, this.userId)
    //   .subscribe((response: any) => {
    //     console.log(response);
    //     this.allFriendExpenses = response.data.expenses;
    //     for (let eachEx of this.allFriendExpenses) {
    //       const expenseDesc = eachEx['expenseDesc'];
    //       console.log();
    //       const payerArr = JSON.parse(eachEx['payer']);

    //       for (let obj of payerArr) {
    //         const x = Object.entries(obj);
    //         // console.log(x);
    //         if (x[0][0] === this.userId) {
    //           this.expenses[expenseDesc] = [x[0][1], eachEx['expenseAmount']];
    //           //data:{biryani:[100,480]}
    //         }
    //       }
    //     }
    //     this.expenses = Object.entries(this.expenses);
    //   });
  }
}
