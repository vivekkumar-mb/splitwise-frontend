import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FriendDataComponent } from './friend-data.component';

describe('FriendDataComponent', () => {
  let component: FriendDataComponent;
  let fixture: ComponentFixture<FriendDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FriendDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FriendDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
