import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SignUpUser } from 'models/signUpUser';
import { HttpserviceService } from '../services/httpservice.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
  constructor(
    private sign: SignUpUser,
    private APIservice: HttpserviceService,
    private router: Router
  ) {}

  ngOnInit(): void {}
  valid: any = false;
  SignUp(username: any, email: any, password: any) {
    if (username == '' && email == '' && password == '') {
      return;
    } else if (
      /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
        email
      )
    ) {
      this.valid = true;
    } else if (!this.valid) {
      alert('You have entered an invalid email address!');
      return;
    }

    console.log(username, email, password);
    this.sign.username = username;
    this.sign.email = email;
    this.sign.password = password;
    this.APIservice.createSignUpPost(this.sign).subscribe((res: any) => {
      console.log(res);
      if (res.message === 'ok') {
        window.alert('You are Successfully Registered..Please LogIn');
        this.router.navigate(['/login']);
      } else {
        window.alert(res.message);
      }
    });
  }
}
