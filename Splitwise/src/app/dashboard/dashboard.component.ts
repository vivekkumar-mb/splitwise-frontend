import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { User } from '../models/user';
import { HttpserviceService } from '../services/httpservice.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  public username = 'sumit';
  public email: any;
  constructor(
    private router: Router,
    private userService: HttpserviceService,
    public user: User,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.email = localStorage.getItem('email');
      this.userService.getUserData(this.email).subscribe((response) => {
        console.log(response);
        this.user.friends = response.friends;
        this.user.finalSettlement = response.finalSettlement;
        this.user.groups = response.groups.filter((group: any) => {
          return !group[1].includes(' and ');
        });
        console.log(this.user.groups);
        this.user.username = response.username;
      });
    });
    // this.email = localStorage.getItem('email');
    // this.userService.getUserData(this.email).subscribe((response) => {
    //   console.log(response);
    //   this.user.friends = response.friends;
    //   this.user.finalSettlement = response.finalSettlement;
    //   this.user.groups = response.groups.filter((group: any) => {
    //     return !group[1].includes(' and ');
    //   });
    //   console.log(this.user.groups);
    //   this.user.username = response.username;

    // });
  }

  getDashboardData() {
    this.router.navigate(['/logged/dashboard']);
  }

  getAllExpenses() {
    this.router.navigate(['/logged/all']);
  }

  getGroup(groupId: string) {
    this.router.navigate(['logged/group', groupId]);
  }

  getFriend(friend: any) {
    this.router.navigate(['logged/friend', friend[0], friend[1]]);
  }

  addExpenseData(amount: any, desc: any, friend: any) {
    const userId = localStorage.getItem('userId');
    this.userService
      .postFriendExpense(userId, amount, desc, friend)
      .subscribe((res: any) => {
        console.log(res);
        window.alert('Expense Added Successfully!');
        this.router.navigate(['logged']);
      });
    console.log(amount, desc, friend);
  }
  addFriendData(email: any) {
    const userId = localStorage.getItem('userId');
    this.userService.postAddFriend(userId, email).subscribe((res: any) => {
      window.alert(res.message);
      console.log(res);
      this.email = localStorage.getItem('email');
      this.userService.getUserData(this.email).subscribe((response) => {
        console.log(response);
        this.user.friends = response.friends;
        this.user.finalSettlement = response.finalSettlement;
        this.user.groups = response.groups.filter((group: any) => {
          return !group[1].includes(' and ');
        });
        console.log(this.user.groups);
        this.user.username = response.username;
      });
    });
    console.log(email);
  }
}
