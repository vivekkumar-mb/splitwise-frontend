import { Injectable } from '@angular/core';
import { UserLogin } from 'models/userLogin';
import { Observable, throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { SignUpUser } from 'models/signUpUser';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class HttpserviceService {
  public email: any;
  constructor(private http: HttpClient, private router: Router) {}

  serverUrl: string = 'http://localhost:3000/';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  createLoginPost(login: UserLogin) {
    return this.http
      .post<UserLogin>(
        this.serverUrl + 'api/user/login',
        login,
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.handleError));
  }
  createSignUpPost(signup: SignUpUser) {
    return this.http
      .post<SignUpUser>(
        this.serverUrl + 'api/user/register',
        signup,
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.handleError));
  }

  getUserData(email: string): Observable<any> {
    return this.http.get<any>(this.serverUrl + 'api/user/' + email).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  getAllExpenses(email: string): Observable<any> {
    return this.http
      .get<any>(this.serverUrl + 'api/user/allExpenses/' + email)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  getGroupAllExpenses(groupId: string): Observable<any> {
    return this.http
      .get<any>(this.serverUrl + 'api/user/group/' + groupId)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  getFriendAllExpenses(friendId: string, userId: string): Observable<any> {
    return this.http
      .get<any>(this.serverUrl + 'api/user/friend/' + userId + '/' + friendId)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }
  postFriendExpense(userId: any, amount: any, desc: any, friend: any) {
    console.log({
      friendID: friend,
      userID: userId,
      expenseDesc: desc,
      expenseAmount: amount,
    });
    return this.http
      .post<any>(
        this.serverUrl + 'api/user/addExpense/friend',
        {
          friendID: friend,
          userID: userId,
          expenseDesc: desc,
          expenseAmount: amount,
        },
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.handleError));
  }
  postAddFriend(userId: any, email: any) {
    return this.http
      .post<SignUpUser>(
        this.serverUrl + 'api/user/addFriend',
        { userID: userId, friendEmail: email },
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.handleError));
  }
  loggedIn() {
    this.email = localStorage.getItem('email');
    return !!(localStorage.getItem('email') && localStorage.getItem('token'));
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['']);
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code. // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    } // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }
}
