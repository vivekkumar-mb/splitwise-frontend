import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SignUpUser } from 'models/signUpUser';
import { UserLogin } from 'models/userLogin';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardOverviewComponent } from './dashboard-overview/dashboard-overview.component';
import { AllExpenseComponent } from './all-expense/all-expense.component';
import { HttpserviceService } from './services/httpservice.service';
import { User } from './models/user';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './home/header/header.component';
import { FooterComponent } from './home/footer/footer.component';
import { GroupDataComponent } from './group-data/group-data.component';
import { GroupData } from './models/groupData';
import { FriendDataComponent } from './friend-data/friend-data.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    DashboardOverviewComponent,
    AllExpenseComponent,
    SignupComponent,
    LoginComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    GroupDataComponent,
    FriendDataComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [HttpserviceService, User, SignUpUser, UserLogin, GroupData],
  bootstrap: [AppComponent],
})
export class AppModule {}
