import { Component, OnInit } from '@angular/core';
import { HttpserviceService } from '../services/httpservice.service';
import { User } from '../models/user';

@Component({
  selector: 'app-dashboard-overview',
  templateUrl: './dashboard-overview.component.html',
  styleUrls: ['./dashboard-overview.component.css'],
})
export class DashboardOverviewComponent implements OnInit {
  public totalbalance: any = 0;
  public youOwe: any = 0;
  public youAreOwed: Number = 0;
  public negativeSum: any = {};
  public positiveSum: any = {};
  public friendsObject: any = {};
  public email: any;
  constructor(private userService: HttpserviceService, public user: User) {}

  ngOnInit(): void {
    this.email = localStorage.getItem('email');
    this.userService.getUserData(this.email).subscribe((response) => {
      this.user.friends = response.friends;
      this.user.finalSettlement = response.finalSettlement;
      this.user.groups = response.groups.filter((group: any) => {
        return !group[1].includes(' and ');
      });
      this.user.username = response.username;
      for (let index of this.user.friends) {
        this.friendsObject[index[0]] = index[1];
      }
      console.log(this.user);
      for (let values in this.user.finalSettlement) {
        if (this.user.finalSettlement[values] < 0) {
          this.youOwe += this.user.finalSettlement[values];
          this.negativeSum[values] = this.user.finalSettlement[values];
        } else {
          this.youAreOwed += this.user.finalSettlement[values];
          this.positiveSum[values] = this.user.finalSettlement[values];
        }
        this.totalbalance += this.user.finalSettlement[values];
      }
      this.negativeSum = Object.entries(this.negativeSum);
      this.positiveSum = Object.entries(this.positiveSum);
    });
  }
}
