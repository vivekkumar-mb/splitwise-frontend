import { Component, OnInit } from '@angular/core';
import { HttpserviceService } from '../services/httpservice.service';

@Component({
  selector: 'app-all-expense',
  templateUrl: './all-expense.component.html',
  styleUrls: ['./all-expense.component.css'],
})
export class AllExpenseComponent implements OnInit {
  public allExpenseData: any;
  public email: any;
  constructor(private apiService: HttpserviceService) {}

  ngOnInit(): void {
    this.email = localStorage.getItem('email');
    this.apiService.getAllExpenses(this.email).subscribe((response) => {
      this.allExpenseData = Object.entries(response.data);
      console.log(this.allExpenseData);
    });
  }
}
