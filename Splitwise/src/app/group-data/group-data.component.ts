import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { GroupData } from '../models/groupData';
import { HttpserviceService } from '../services/httpservice.service';

@Component({
  selector: 'app-group-data',
  templateUrl: './group-data.component.html',
  styleUrls: ['./group-data.component.css'],
})
export class GroupDataComponent implements OnInit {
  public groupId: any;
  public userId: any;
  public expenses: any = {};
  constructor(
    private route: ActivatedRoute,
    private apiService: HttpserviceService,
    public groupData: GroupData
  ) {}

  ngOnInit(): void {
    // this.groupId = this.route.snapshot.params.groupId;
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.groupId = params.get('groupId');
    });
    this.userId = localStorage.getItem('userId');
    this.apiService.getGroupAllExpenses(this.groupId).subscribe((response) => {
      console.log(response);
      this.groupData.name = response.data.groupName;
      this.groupData.expenses = response.data.expenses;
      console.log(this.groupData.name, this.groupData.expenses);
      for (let eachEx of this.groupData.expenses) {
        const expenseDesc = eachEx['expenseDesc'];
        console.log();
        const payerArr = JSON.parse(eachEx['payer']);

        for (let obj of payerArr) {
          const x = Object.entries(obj);
          // console.log(x);
          if (x[0][0] === this.userId) {
            this.expenses[expenseDesc] = [x[0][1], eachEx['expenseAmount']];
            //data:{biryani:[100,480]}
          }
        }
      }
      this.expenses = Object.entries(this.expenses);
      console.log(this.expenses);
    });
  }
}
